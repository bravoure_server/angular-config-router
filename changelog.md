## Angular Config Router / Bravoure component

This Component generates the router and creates every single page.

### **Versions:**

1.0.4 - Fix for building
1.0.3 - Updated name on file extend-on-enter.factory.js
1.0.2 - Added external-on-enter.factory.js file to correctly extend functionalities in the router by project
1.0.1 - Updated redirection if enter the error page when the sugpage is offline
1.0.0 - Initial Stable version

---------------------
