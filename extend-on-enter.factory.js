(function () {
    'use strict';

    // Function to be extended
    function extendOnEnter ($rootScope) {
        return {
            extraFunction: function () {
            }
        }
    }

    extendOnEnter.$inject = ['$rootScope'];

    angular
        .module('bravoureAngularApp')
        .factory('extendOnEnter', extendOnEnter);

})();
