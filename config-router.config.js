
(function () {
    'use strict';

    function configBravoure (
        $stateProvider,
        $urlRouterProvider,
        APP_CONFIG,
        PATH_CONFIG,
        $urlMatcherFactoryProvider
    ) {

        $urlMatcherFactoryProvider.strictMode(true);

        // We create an array to store the ids of each identifier
        var arrayRouting = [];

        // Iterated the config response
        for (var i = 0; i < APP_CONFIG.length; i ++) {
            /* Get the routes from the config */
            if (APP_CONFIG[i].content.content_type == 'routes') {
                PATH_CONFIG.routing = APP_CONFIG[i].content.routes;
            }
            /* Get the current language*/
            if (APP_CONFIG[i].content.content_type == 'language') {
                PATH_CONFIG.current_language = APP_CONFIG[i].content.language;
            }
            /* Get the current locations for images */
            if (APP_CONFIG[i].content.content_type == 'storage') {
                var locations = APP_CONFIG[i].content.locations;
                PATH_CONFIG.locations = [];
                for (var j = 0; j < locations.length; j++) {
                    PATH_CONFIG.locations[locations[j]['name']] = locations[j]['location'];
                }
            }
        }

        setTimeout(function () {

            $stateProvider
                .state('top', {
                    name: 'top',
                    url: '',
                    views: {
                        'top': {
                            template: '<div ui-view="main" />'
                        }
                    },
                    deepStateRedirect: { default: "top.homepage" },
                    sticky: true
                });

            for (var key in PATH_CONFIG.routing) {

                var key = PATH_CONFIG.routing[key];

                arrayRouting[key.identifier] = key.id;

                // sets the 404 page from the API
                if (typeof key.route == 'undefined' || 'top.' + key.identifier == PATH_CONFIG.ERROR_IDENTIFIER) {

                    $stateProvider
                        .state('top.' + key.identifier, {
                            templateUrl: 'app/templates/loader.template.html',
                            views: {
                                'main': {
                                    template: '<loader-template></loader-template>',
                                    controller: function pageController ($scope, $rootScope, $stateParams) {
                                        var themeDefined = false;

                                        $rootScope.showMenu = true;

                                        // defines a special theme form parameters.js
                                        if (typeof data.specialTheme != 'undefined') {
                                            for (var i = 0; i < data.specialTheme.length; i ++) {
                                                $rootScope.theme = (typeof data.specialTheme[i][$stateParams.identifier] != 'undefined') ? data.specialTheme[i][$stateParams.identifier] : $rootScope.theme;
                                                themeDefined = true;
                                            }
                                        }

                                        if (!themeDefined) {
                                            $rootScope.theme = data.theme;
                                        }

                                        // Defines the Theme from the settings call
                                        for (var i = 0; i < APP_CONFIG.length; i ++) {
                                            if (APP_CONFIG[i].content.content_type == 'theme') {
                                                $rootScope.theme = 'theme-' + APP_CONFIG[i].content.colors.color_schema;
                                            }
                                        }
                                        // loads the theme by page, if it is found in parameters,
                                        // in array "specialTheme"
                                        $rootScope.$emit('loadTheme', $rootScope.theme);

                                    }
                                }
                            },

                            params: {
                                identifier: key.identifier,
                                url: key.url
                            }
                        });

                }

                // Creation of different variables to provide to the router if the route is not a redirection
                if (typeof key.redirect_object == 'undefined' && key.route != '//') {

                    if (typeof key.route != 'undefined' && 'top.' + key.identifier != PATH_CONFIG.ERROR_IDENTIFIER ) {

                        // Check if the pages are not in the blacklist from parameters
                        if (data.pagesNotInRouter.indexOf(key.identifier ) == -1) {

                            var actualKey = key;
                            $stateProvider
                                .state('top.' + actualKey.identifier, {
                                    name: 'top.' + actualKey.identifier,
                                    url: '/' + PATH_CONFIG.current_language + key.route,

                                    sticky: true,
                                    views: {
                                        'main': {
                                            template: '<loader-template></loader-template>',
                                            controller: function pageController ($scope, $rootScope, $stateParams, $timeout, onEnterConfig) {

                                                // Enables the scrollTop on loading a new page
                                                if (data.scrollTop && $stateParams.identifier != 'top.homepage') {
                                                    $timeout(function () {
                                                        $('body, html').animate({
                                                            scrollTop: 0
                                                        }, 0);
                                                    }, 0);
                                                }

                                                onEnterConfig.loadingContent();

                                            }
                                        }
                                    },
                                    onEnter: onEnterConfig,
                                    params: {
                                        identifier: 'top.' + key.identifier,
                                        url: key.url,
                                        route: '/' + PATH_CONFIG.current_language + key.route
                                    }
                                });

                        }
                    }
                }
            }


            $urlRouterProvider.rule(function($injector, $location) {
                var path = $location.path();
                var $rootScope = $injector.get('$rootScope');
                var $cookies = $injector.get('$cookies');
                if ($location.$$url == '') {

                    // Sets the Prerender Status Code to 301
                    $rootScope.preRender301Status = true;

                    return path + '/';
                }

            });


            // Sets the redirection to the 404 page
            $urlRouterProvider.otherwise(function($injector){
                var $state = $injector.get('$state');
                var $window = $injector.get('$window');
                var $rootScope = $injector.get('$rootScope');
                var $location = $injector.get('$location');
                var $http = $injector.get('$http');
                var API_Service = $injector.get('API_Service');
                var $q = $injector.get('$q');
                var $translate = $injector.get('$translate');
                var redirectService = $injector.get('redirectService');

                // if root, selects the language where to redirect to the homepage
                if ($location.$$path == '/' || $location.$$path == '/index.html') {

                    // Sets the Prerender Status Code to 302
                    $rootScope.preRender302Status = 302;

                    // Keeps the search parameters when the language is not set, for the root url
                    if ($location.$$url != '/') {
                        return $location.path('/' + $translate.preferredLanguage() + '/' );
                    } else {

                        // If a redirection happened (for example the subpage is offline,
                        // it was redirected tot the error page,
                        // and it wenters here.
                        if ($state.params.identifier != PATH_CONFIG.ERROR_IDENTIFIER) {
                            return $state.go(PATH_CONFIG.HOME_IDENTIFIER, {}, {
                                location: 'replace',
                                inherit: false
                            });
                        }
                    }

                    // redirects to the 404
                } else {

                    var themeDefined = false;

                    // defines a special theme form parameters.js
                    if (typeof data.specialTheme != 'undefined') {
                        for (var i = 0; i < data.specialTheme.length; i ++) {
                            $rootScope.theme = (typeof data.specialTheme[i][$stateParams.identifier] != 'undefined') ? data.specialTheme[i][$stateParams.identifier] : $rootScope.theme;
                            themeDefined = true;
                        }
                    }

                    if (!themeDefined) {
                        $rootScope.theme = data.theme;
                    }

                    // loads the theme by page, if it is found in parameters,
                    // in array "specialTheme"
                    $rootScope.$emit('loadTheme', $rootScope.theme);

                    redirectService.redirectCall();

                }

            });

        }, 0);

        function onEnterConfig (extendOnEnter) {

            // Adding class to be extended correctly adding it in jello_components_extended folder
            extendOnEnter.extraFunction();
        }

        onEnterConfig.$inject = ['extendOnEnter'];

    }

    configBravoure.$inject = [
        '$stateProvider',
        '$urlRouterProvider',
        'APP_CONFIG',
        'PATH_CONFIG',
        '$urlMatcherFactoryProvider'
    ];

    angular
        .module('bravoureAngularApp')
        .config(configBravoure);

})();
